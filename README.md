# PostZipQuery-method-in-Java



import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;

public class PostZipQuery {
 
 public static void main(String[] args) throws IOException{
 
     // concatenate all command line arguments
     
     String input;
     if (args.length > 0)
     {
       input = args[0];
       for (int i = 1; i < args.length; i++)
             input+= " " + args[i];
     }
     else
       input = "90210";
      
      // establesh URL connection
      
      URL u = new URL("http://www.usps.gov/cgi-bin/zip4/ctystzip");
      URLConnection connection = u.openConnection();
      
      // send post data
      connection.setDoOutput(true);
      OutputStream out = connection.getOutputStream();
      PrintWriter writer = new PrintWriter(out);
      writer.print("ctystzip=" + URLEncoder.encode(input) + "\n");
      writer.close();
      
      // print server response
      
      InputStream in = connection.getInputStream();
      BufferedReader reader = new BufferedReader(new InputStreamReader(in));
      
      boolean done = false;
      
      while (!done)
      {
        String inputLine = reader.readLine();
        if (inputLine == null)
          done = true;
         else
           System.out.println(inputLine);       
      }
     reader.close();
   }
 
 }     
      
      
       
     
